//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#ifndef __smtk_simulation_adh_ExportHotStartFile_h
#define __smtk_simulation_adh_ExportHotStartFile_h

#include "smtk/simulation/adh/Exports.h"

#include "smtk/mesh/core/Resource.h"

#include <pybind11/pybind11.h>

#include <string>

namespace smtk
{
namespace simulation
{
namespace adh
{

/// A functor for generating an AdH .hot file from smtk cell data.
///
/// This functor is designed to be called from python within the export process.
/// As such, it accepts an export "spec" python object commonly used during SMTK
/// exports to contain the state of the export.
class SMTKADHSIMULATION_EXPORT ExportHotStartFile
{
public:
  // Python entrypoint
  void operator()(pybind11::object spec);

  // C++ entrypoint
  void operator()(const std::string& meshHotStartFileName,
                  smtk::mesh::ResourcePtr& meshResource,
                  double startTime,
                  std::size_t startTimeUnits);
};

}
}
}

#endif
