cmake_minimum_required(VERSION 3.8.2)

# Include GNU install directory module to detect where to install
# files on Linux/Unix systems (e.g., lib vs lib64)
include(GNUInstallDirs)

#-----------------------------------------------------------------------------
# Plugin name and version

project(adh-extensions VERSION 1.0)

if (NOT BUILD_SHARED_LIBS)
  # Static builds need `-fPIC` due to the force-shared nature of the Python
  # module.
  set(CMAKE_POSITION_INDEPENDENT_CODE ON)
endif ()

# Set the directory where cmake configuration files are installed. The
# convention for this directory's location is OS-dependent. See
# https://cmake.org/cmake/help/v3.13/command/find_package.html section "Search
# Procedure" for more information.
set(INSTALL_CONFIG_DIR ${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}/${PROJECT_VERSION})
if (WIN32)
  set(SMTK_INSTALL_CONFIG_DIR cmake/${PROJECT_NAME}/${PROJECT_VERSION})
endif()

#-----------------------------------------------------------------------------
# Find packages for the plugin
find_package(smtk REQUIRED)

#-----------------------------------------------------------------------------
# Boost settings

add_library(boost_cxx_flags INTERFACE)
# For MSVC:
#  (/EHsc) setup windows exception handling
#  (/wd4996) quiet warnings about printf being potentially unsafe
#  (/wd4503) quiet warnings about truncating decorated name
#  (-DBOOST_ALL_NO_LIB) remove autolinking
target_compile_options(boost_cxx_flags INTERFACE
  $<$<CXX_COMPILER_ID:MSVC>:/EHsc /wd4996 /wd4503 -DBOOST_ALL_NO_LIB>)

set_property(TARGET Boost::boost APPEND PROPERTY INTERFACE_LINK_LIBRARIES boost_cxx_flags)

#-----------------------------------------------------------------------------
# VXL settings

include(CMakeDependentOption)
cmake_dependent_option(ENABLE_VXL_SUPPORT "Build VXL component" OFF
  "SMTK_ENABLE_PARAVIEW_SUPPORT;SMTK_ENABLE_QT_SUPPORT;SMTK_ENABLE_VTK_SUPPORT" OFF)

#-----------------------------------------------------------------------------
# Python settings

# Initialize PYTHON_MODULEDIR.
# This stores the location where we'll install our Python modules.
# Note that PYTHON_MODULEDIR may be provided to override this behavior.
if (NOT DEFINED PYTHON_MODULEDIR)
  if (INSTALL_PYTHON_TO_SITE_PACKAGES)
    execute_process(
      COMMAND
      ${PYTHON_EXECUTABLE}
      -c "import site; print(site.getsitepackages())[-1]"
      RESULT_VARIABLE PYTHON_MODULEDIR
      )
  elseif(WIN32)
    set(PYTHON_MODULEDIR
      "${CMAKE_INSTALL_BINDIR}/Lib/site-packages")
  else()
    set(PYTHON_MODULEDIR
      "${CMAKE_INSTALL_LIBDIR}/python${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}/site-packages")
  endif()
endif()

#-----------------------------------------------------------------------------
# Pybind11 settings

set(PYBIND11_FLAGS "")
if(CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR
    CMAKE_CXX_COMPILER_ID MATCHES "GNU" OR
    CMAKE_CXX_COMPILER_ID MATCHES "Intel")
  string(APPEND PYBIND11_FLAGS " -Wno-shadow")
endif()

# Set the directory where the binaries will be stored
include(GNUInstallDirs)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_BINDIR})
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_LIBDIR})
set(CMAKE_INSTALL_CMAKEDIR "${CMAKE_INSTALL_LIBDIR}/cmake/${PROJECT_NAME}/${PROJECT_VERSION}")

# Add our Cmake directory to the module search path
list(APPEND CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMake)

#-----------------------------------------------------------------------------
# Plugin tests

option(ENABLE_TESTING "Enable Testing" OFF)
if (ENABLE_TESTING)
  include(CTest)
  include(TestingMacros)
endif()

#-----------------------------------------------------------------------------
# Plugin sources

# Include the plugin's source directory
add_subdirectory(smtk)

#-----------------------------------------------------------------------------
# Plugin configuration

string(REPLACE "-" "_" safe_project_name "${PROJECT_NAME}")

if (SMTK_ENABLE_PARAVIEW_SUPPORT)
  # Build the plugin containing the File menu items
  paraview_plugin_scan(
    PLUGIN_FILES smtk/mesh/adh/plugin/paraview.plugin
    PROVIDES_PLUGINS paraview_plugins
    ENABLE_BY_DEFAULT ON
    HIDE_PLUGINS_FROM_CACHE ON)
  paraview_plugin_build(
    LIBRARY_SUBDIRECTORY "smtk-${smtk_VERSION}"
    PLUGINS ${paraview_plugins}
    PLUGINS_FILE_NAME "smtk.adhextensions.xml"
    AUTOLOAD ${paraview_plugins}
    INSTALL_EXPORT ${PROJECT_NAME}-plugin
    HEADERS_DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}"
    CMAKE_DESTINATION "${CMAKE_INSTALL_CMAKEDIR}"
    ADD_INSTALL_RPATHS ON
    TARGET ${safe_project_name}_paraview_plugins)
endif()

export(
  EXPORT      ${PROJECT_NAME}
  FILE        "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR}/${PROJECT_NAME}-targets.cmake")
install(
  EXPORT      ${PROJECT_NAME}
  DESTINATION "${CMAKE_INSTALL_CMAKEDIR}"
  FILE        "${PROJECT_NAME}-targets.cmake")

include(CMakePackageConfigHelpers)

# Our requirements for a version file are basic, so we use CMake's basic version
# file generator
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}-config-version.cmake"
  VERSION ${${PROJECT_NAME}_VERSION}
  COMPATIBILITY AnyNewerVersion)

configure_file(
  "${CMAKE_CURRENT_SOURCE_DIR}/CMake/smtk-plugin-config.cmake.in"
  "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR}/${PROJECT_NAME}-config.cmake"
  COPYONLY)
install(
  FILES       "${CMAKE_BINARY_DIR}/${CMAKE_INSTALL_CMAKEDIR}/${PROJECT_NAME}-config.cmake"
  DESTINATION "${CMAKE_INSTALL_CMAKEDIR}")
