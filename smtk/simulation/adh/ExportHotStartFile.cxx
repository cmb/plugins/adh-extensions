//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================

#include "smtk/simulation/adh/ExportHotStartFile.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"

#include "smtk/mesh/core/PointField.h"

//force to use filesystem version 3
#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>

#include <fstream>

namespace
{

template <typename Type>
Type getPyAttribute(pybind11::object& object, const std::string& name)
{
  if (!pybind11::hasattr(object, name.c_str()))
  {
    std::cerr << "could not find " << name << std::endl;
    return Type();
  }
  return object.attr(name.c_str()).cast<Type>();
}

template <typename T>
void writePointField(std::fstream& hotStartFile, const smtk::mesh::PointField& pointField)
{
  std::vector<T> retrievedFieldValues = pointField.get<T>();

  std::size_t nPoints = pointField.size();
  std::size_t dimension = pointField.dimension();
  for (std::size_t i = 0; i < nPoints; ++i)
  {
    for (std::size_t j = 0; j < dimension; ++j)
    {
      if (i != 0)
      {
        hotStartFile << " ";
      }
      hotStartFile << retrievedFieldValues[i * pointField.dimension() + j];
    }
    hotStartFile << std::endl;
  }
  hotStartFile << "ENDDS" << std::endl;
}

}

namespace smtk
{
namespace simulation
{
namespace adh
{

void ExportHotStartFile::operator()(pybind11::object py_scope)
{
  // Access the attribute resource describing the simulation
  auto sim_atts = getPyAttribute<smtk::attribute::ResourcePtr>(py_scope, "sim_atts");

  double startTimeValue = 0.;
  {
    auto time_att = sim_atts->findAttribute("Time");
    auto item = std::dynamic_pointer_cast<smtk::attribute::DoubleItem>(
      time_att->itemAtPath("StartTime/Value"));
    startTimeValue = item->value();
  }

  std::size_t startTimeUnits;
  {
    auto time_att = sim_atts->findAttribute("Time");
    auto item = std::dynamic_pointer_cast<smtk::attribute::IntItem>(
      time_att->itemAtPath("StartTime/Units"));
    startTimeUnits = static_cast<std::size_t>(item->value());
  }

  // Access the mesh resource describing the simulation mesh
  auto mesh_collection = getPyAttribute<smtk::mesh::ResourcePtr>(py_scope, "mesh_collection");

  // Compose the geometry and boundary condition ouput file names
  auto output_filebase = getPyAttribute<std::string>(py_scope, "output_filebase");
  auto output_directory = getPyAttribute<std::string>(py_scope, "output_directory");

  boost::filesystem::path hotStartFile =
    boost::filesystem::path(output_directory) / output_filebase;
  hotStartFile += ".hot";

  return operator()(hotStartFile.string(), mesh_collection, startTimeValue, startTimeUnits);
}

void ExportHotStartFile::operator()(
  const std::string& meshHotStartFileName,
  smtk::mesh::ResourcePtr& meshResource,
  double startTime,
  std::size_t startTimeUnits)
{
  // Construct a filestream for the geometry. The contents of this file are
  // generated entirely by this method, so we rewrite the file each time the
  // method is called.
  std::fstream hotStartFile(meshHotStartFileName.c_str(), std::ios::out | std::fstream::trunc);

  // Access the top-level meshset's point fields
  std::set<smtk::mesh::PointField> pointFields = meshResource->meshes().pointFields();

  // We are only interested in point fields that match the allowed hotstart
  // names, so we remove any extraneous fields.
  std::set<std::string> allowedNames = { { "IH", "IT", "ioh", "iov", "icon" } };
  for (auto it = pointFields.begin(); it != pointFields.end(); )
  {
    if (allowedNames.find(it->name()) == allowedNames.end())
    {
      it = pointFields.erase(it);
    }
    else
    {
      ++it;
    }
  }

  // For each point field...
  for (const smtk::mesh::PointField& pointField : pointFields)
  {
    // ...fill in the file's preamble...
    hotStartFile << "DATASET" << std::endl;
    hotStartFile << "OBJTYPE \"mesh3d\"" << std::endl;
    hotStartFile << "BEGSCL" << std::endl;
    hotStartFile << "ND " << meshResource->meshes().points().size() << std::endl;
    hotStartFile << "NC " << meshResource->meshes().cells().size() << std::endl;
    hotStartFile << "NAME " << pointField.name() << std::endl;
    hotStartFile << "TS " << startTimeUnits << " " << startTime << std::endl;

    //...and write the type-specific field data to file.
    if (pointField.type() == smtk::mesh::FieldTypeFor<double>::type)
    {
      writePointField<double>(hotStartFile, pointField);
    }
    else if (pointField.type() == smtk::mesh::FieldTypeFor<int>::type)
    {
      writePointField<int>(hotStartFile, pointField);
    }
  }

  hotStartFile.close();
}

}
}
}
