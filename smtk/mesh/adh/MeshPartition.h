//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#ifndef __smtk_mesh_adh_MeshPartition_h
#define __smtk_mesh_adh_MeshPartition_h

#include "smtk/mesh/adh/Exports.h"

#include "smtk/attribute/Attribute.h"

#include "smtk/mesh/core/CellSet.h"
#include "smtk/mesh/core/DimensionTypes.h"
#include "smtk/mesh/core/MeshSet.h"
#include "smtk/mesh/core/Resource.h"

#include "smtk/model/Resource.h"

#include <string>
#include <vector>

namespace smtk
{
namespace mesh
{
namespace adh
{

/// A struct for holding a partitioned mesh.
///
/// Because AdH files have a local ordering that must be consistent across
/// multiple file descriptions, we use a common data structure for AdH
/// operations.
struct SMTKADHMESH_EXPORT MeshPartition
{
  MeshPartition(const smtk::mesh::MeshSet& ms, int partitionId, smtk::mesh::DimensionType dim);

  int id() const { return m_partitionId; }

  /// Return number cells of the set dimension
  std::size_t numCells() const;

  /// Return all the cells of a given type
  smtk::mesh::CellSet cells(const smtk::mesh::CellType& type) const;

  /// Return all the cells, restricted to the dimension passed in during
  /// construction
  smtk::mesh::CellSet cells() const;

  smtk::mesh::MeshSet m_meshSet;
  smtk::mesh::DimensionType m_dim;
  int m_partitionId;
};

/// Return a vector of mesh partitions, where each partition corresponds to a
/// classified model entity.
SMTKADHMESH_EXPORT
std::vector<MeshPartition> partitionByModelEntity(smtk::mesh::ResourcePtr meshResource,
                                                  smtk::mesh::DimensionType type);

/// Return a vector of mesh partitions, where each partition corresponds to a
/// classified model entity with the given model property.
SMTKADHMESH_EXPORT
std::vector<MeshPartition> partitionByModelProperty(smtk::mesh::ResourcePtr meshResource,
                                                    smtk::model::ResourcePtr resource,
                                                    const std::string& modelPropertyName,
                                                    smtk::mesh::DimensionType type);

/// Return a vector of mesh partitions, where each partition corresponds to a
/// classified model entity associated to each of the input attributes.
SMTKADHMESH_EXPORT
std::vector<MeshPartition> partitionByAttributeAssociation(
  smtk::mesh::ResourcePtr meshResource,
  const std::set<smtk::attribute::AttributePtr>& attributes,
  smtk::mesh::DimensionType type = DimensionType_MAX);

}
}
}

#endif
