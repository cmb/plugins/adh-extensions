//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/mesh/adh/Registrar.h"

#include "smtk/mesh/adh/operators/AnnotateCanonicalIndex.h"
#include "smtk/mesh/adh/operators/GenerateHotStartData.h"

namespace smtk
{
namespace mesh
{
namespace adh
{
namespace
{
typedef std::tuple<AnnotateCanonicalIndex, GenerateHotStartData> OperationList;
}

void Registrar::registerTo(const smtk::operation::Manager::Ptr& operationManager)
{
  operationManager->registerOperations<OperationList>();
}

void Registrar::unregisterFrom(const smtk::operation::Manager::Ptr& operationManager)
{
  operationManager->unregisterOperations<OperationList>();
}

}
}
}
