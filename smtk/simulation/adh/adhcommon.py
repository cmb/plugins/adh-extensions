#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================
"""
Common functions for all ADH exporters
* surface water, aka shallow water 2D, aka ADH 2D
* shallow water
* ground water, aka GEOTACS, aka CTB

"""
import functools
import os
import shutil
import uuid
import smtk
import sys
if sys.version_info >= (3, 0):
    from importlib import reload
reload(smtk)
if smtk.wrappingProtocol() == 'pybind11':
  import smtk.attribute
  import smtk.io
  import smtk.mesh
  import smtk.model
  import smtk.simulation

import smtksimulationadh

# Originally, functions in this module were called in sequence within a scope
# where the output file was kept open. Now that we use an admixture of Python
# and C++ calls to populate cards, we must encapsulate our I/O functions to
# prevent leaking the file state between different functions.
def output_writer(func):
    @functools.wraps(func)
    def wrapped(scope, *args, **kwargs):
      if hasattr(scope, 'output'):
        if not scope.output.closed:
            scope.output.close()
        with open(scope.output.name, 'a+') as scope.output:
            return func(scope, *args, **kwargs)
      else:
        return func(scope, *args, **kwargs)
    return wrapped

class CardType:
  '''Enumeration of ADH card types
  '''
  VAL   = 'val'    # value
  IDVAL = 'idval'  # id plus value
  BC    = 'bc'     # boundary condition format
  MULTIVAL = 'multival'  # output spans multiple items
  CONBC = 'conbc'   # "constituent" BC (i.e., includes constituent index)
  IDCONVAL = 'idconval'  # material item that references consituent with a value


# ---------------------------------------------------------------------
class CardFormat:
  '''Format information for one card and corresponding attribute item
  '''
  def __init__(self, item_name, opcode, comment, subitem_names, item_path, custom_writer):
    self.card_type = None
    self.item_name = item_name
    self.opcode = opcode
    self.comment = comment
    self.subitem_names = subitem_names
    self.custom_writer = custom_writer
    self.item_path = item_path

  @classmethod
  def val(cls, item_name, opcode, comment=None, subitem_names=None, \
      item_path = None, custom_writer=None):
    '''Returns CardFormat instance initialized as type VAL
    '''
    info = cls(item_name, opcode, comment, subitem_names, item_path, custom_writer)
    info.card_type = CardType.VAL
    return info

  @classmethod
  def idval(cls, item_name, opcode, comment=None, subitem_names=None, \
      item_path=None, custom_writer=None):
    '''Returns CardFormat instance initialized as type IDVAL
    '''
    info = cls(item_name, opcode, comment, subitem_names, item_path, custom_writer)
    info.card_type = CardType.IDVAL
    return info

  @classmethod
  def bc(cls, item_name, opcode, comment=None, subitem_names=None, \
      item_path=None, custom_writer=None):
    '''Returns CardFormat instance initialized as type BC
    '''
    if opcode is None:
      opcode = item_name
    info = cls(item_name, opcode, comment, subitem_names, item_path, custom_writer)
    info.card_type = CardType.BC
    return info

  @classmethod
  def multival(cls, item_names, opcode, comment=None, subitem_names=None, item_path=None):
    '''Returns CardFormat instance initialized as type MULTIVAL

    Note that "item_names" arg should be a LIST.
    And custom_writer not currently supported
    '''
    subitem_names = None
    custom_writer = None
    info = cls(item_names, opcode, comment, subitem_names, item_path, custom_writer)
    info.card_type = CardType.MULTIVAL
    return info

  @classmethod
  def conbc(cls, item_name, opcode, comment=None, subitem_names=None, \
      item_path=None, custom_writer=None):
    '''Returns CardFormat instance initialized as type CONBC
    '''
    info = cls(item_name, opcode, comment, subitem_names, item_path, custom_writer)
    info.card_type = CardType.CONBC
    return info

  @classmethod
  def idconval(cls, item_name, opcode, comment=None, subitem_names=None, \
      item_path=None, custom_writer=None):
    '''Returns CardFormat instance initialized as type IDCONVAL
    '''
    info = cls(item_name, opcode, comment, subitem_names, item_path, custom_writer)
    info.card_type = CardType.IDCONVAL
    return info


# ---------------------------------------------------------------------
ExportScope = type('ExportScope', (object,), dict())
def init_scope(spec):
  '''Returns ExportScope object initialized to input spec

  '''
  # Construct a new export scope to populate
  scope = ExportScope()

  # Access the simulation attributes assigned to the export operation
  scope.sim_atts = smtk.attribute.Resource.CastTo(spec.find('attributes').value())

  # Access the model assigned to the export operation
  scope.model = smtk.model.Entity.CastTo(spec.find('model').value(0))
  scope.model_resource = scope.model.resource()

  # Set scope properties to defaults
  scope.output_filebase = 'output'  # default
  scope.output_directory = os.getcwd() # default
  scope.analysis_types = list()
  scope.categories = list()
  scope.export_mesh = False
  scope.export_vegetation = spec.find('ExportVegetation').isEnabled()
  # Determine whether we need to export the mesh
  scope.export_mesh = spec.find('ExportMesh').isEnabled()

  if not scope.model:
    raise Exception('ERROR: Model not specified or not found')

  if scope.export_mesh:
    # Access the mesh assigned to the export operation
    scope.mesh_collection = smtk.mesh.Component(spec.find('mesh').value(0)).resource()
    if not scope.mesh_collection:
      raise Exception('ERROR: unable to obtain mesh object for export')

    # Get mesh points - only from 3D entities since that is what gets
    # written to the .3dm file
    scope.mesh_points = scope.mesh_collection.cells(smtk.mesh.Dims3).points()

  # Assign unique ids to all model cells
  # (although only *required* for face entities)
  matid = 'id'

  # Use material_dict to store <material UUID, adh material index>
  # ADH requires material IDs to start at 1 and be consecutive
  # Use UUID for the key
  scope.material_dict = dict()
  material_att_list = scope.sim_atts.findAttributes('Material')

  #if there are no materials defined, assign all volumes to a default material
  if not material_att_list:
    mat_def = scope.sim_atts.findDefinition('Material')
    default_mat = scope.sim_atts.createAttribute('Default_Material', mat_def)
    volumes = scope.model.manager().findEntitiesOfType(smtk.model.VOLUME, True)
    for volume in volumes:
      default_mat.associateEntity(volume)
    material_att_list.append(default_mat)

  #assign consecutive ids starting at 1 for materials
  for material_index in range(len(material_att_list)):
    material_att = material_att_list[material_index]
    if len(material_att.associatedModelEntityIds()) > 0:
      dataI = smtk.simulation.UserDataInt.New()
      dataI.setValue(material_index+1)
      material_att.setUserData("MaterialID",dataI)

  # Output filebase
  filebase_item = spec.find('FileBase')
  if filebase_item is not None:
    scope.output_filebase = filebase_item.value(0)
    # String off ending ".bc", in case it was included
    if scope.output_filebase.endswith('.bc'):
      scope.output_filebase = scope.output_filebase[0:-3]

  dir_item = spec.find('OutputDirectory')
  if dir_item is not None:
    scope.output_directory = dir_item.value(0)

  types_item = spec.find('AnalysisTypes')
  if types_item is not None:
    for i in range(types_item.numberOfValues()):
      scope.analysis_types.append(types_item.value(i))

  # Make categories set
  categories = set()
  for analysis in scope.analysis_types:
    categories.update(scope.sim_atts.analyses().find(analysis).categories())
  scope.categories = list(categories)

  #Number boundary conditions (1 based) and label the bc att with that id
  bc_att_list = scope.sim_atts.findAttributes('BoundaryCondition')
  bc_att_list.sort(key=lambda bc: bc.name())

  count = 1
  bc_id = 1
  bc_nodal_groups = {}
  bc_face_groups = {}
  for bc_att in bc_att_list:
    #ignore any bcs that aren't part of the categories being exported
    if not bc_att.categories().passes(set(scope.categories)):
      continue

    #Get the list of associated model entities
    model_ent_items = bc_att.associatedModelEntityIds()

    # Skip BCs not associated with any model entities
    if model_ent_items is None:
      continue

    #check to see if this boundary set (nodal or face string) has already
    #been used in another boundary condition. If so, use that id instead
    #of creating a new one.
    exists = False
    if bc_att.definition().isNodal():
      #if the group has already been used, get that bc_id
      if model_ent_items in bc_nodal_groups.values():
        bc_id = list(bc_nodal_groups)[list(bc_nodal_groups.values()).index(model_ent_items)]
        exists = True
      #if it hasn't been used add it to the nodal_groups dictionary
      else:
       bc_id = count
       bc_nodal_groups[bc_id] = model_ent_items
    #Face boundary conditions
    else:
      #if the group has already been used, get that bc_index
      if model_ent_items in bc_face_groups.values():
        bc_id = list(bc_face_groups)[list(bc_face_groups.values()).index(model_ent_items)]
        exists = True
      else:
        bc_id = count
        #if it hasn't been used add it to the face_groups dictionary
        bc_face_groups[bc_id] = model_ent_items

    #add the bc id as an integer property on the bc attribute
    dataI = smtk.simulation.UserDataInt.New()
    dataI.setValue(bc_id)
    bc_att.setUserData("BCID", dataI)

    print('BC ID %d for attribute %s (%d)' % \
        (bc_id, bc_att.name(), bc_att.userData("BCID").value()))
    #only add to the count if there is a new bc set
    if not exists:
      count += 1

  # Define function dict only, initialized adhoc
  scope.function_dict = dict()

  # Second dictionary for constant values that must be specified as AdH function
  scope.constant_function_dict = dict()

  return scope


# ---------------------------------------------------------------------
#
# The following functions adapted from ShallowWaterExporter.py
#
# ---------------------------------------------------------------------


# ---------------------------------------------------------------------
def write_section(scope, att_type):
  '''
  Writes one section of output file
  '''
  print('DEBUG: Writing section for attribute type: %s' % att_type)

  att_list = scope.sim_atts.findAttributes(att_type)
  if att_list is None:
    msg = 'no %s attribute found' % att_type
    print('WARNING:', msg)
    scope.logger.addWarning(msg)
    return False

  # Sort list
  att_list.sort(key=lambda x: x.name())
  for att in att_list:
    #print('att', att.name())
    format_list = scope.format_table.get(att.type())
    if format_list is None:
      msg = 'empty format list for %s' % att.type()
      print('WARNING:', msg)
      scope.logger.addWarning(msg)
      continue

    # Convert single format config to list
    if isinstance(format_list, (CardFormat,)):
      format_list = [format_list]

    if att.definition().associationMask() == 0x0:
      write_items(scope, att, format_list)
      continue

    # (else)
    model_ent_item = att.associatedModelEntityIds()
    if (model_ent_item is None) or (len(model_ent_item) == 0):
      print('Expecting model association for attribute', att.name())
      continue

    # Special handling for material models
    #  - Skip materials not associated with any domains
    #  - Write comment line with attribute name
    if att.type() in ['Material', 'SWMaterial']:
      scope.output.write('! material -- %s\n' % att.name())

    ok = write_items(scope, att, format_list)

  return True


# ---------------------------------------------------------------------
def render_card(scope, item, card_format, context_id=None, group_index=None):
  '''Generates one line (card) of output for input item

  '''
  #print('render_card for item', item.name())

  # Initially generate a list of strings
  output_list = list()
  output_list.append(card_format.opcode)
  if context_id is not None:
    output_list.append(context_id)

  # Append Constituent id for some cards
  constituent_item = None
  if card_format.card_type == CardType.CONBC:
    constituent_id = 0
    att = item.attribute()
    constituent_item = att.find('Constituent')
    constituent = constituent_item.value(0)
    if constituent is None:
      msg = 'No consitituent specified for attribute %s' % att.name()
      print('Warning:', msg)
      scope.logger.addWarning(msg)
    else:
      constituent_id = scope.constituent_dict.get(constituent.id())
    output_list.append(str(constituent_id))
  elif card_format.card_type == CardType.IDCONVAL:
    constituent_item = find_subgroup_item(item, group_index, 'Constituent')
    if constituent_item is None:
      msg = 'No consitituent subitem for %s' % item.name()
      print('Warning:', msg)
      scope.logger.addWarning(msg)
    constituent = constituent_item.value(0)
    if constituent is None:
      msg = 'No consitituent specified for attribute %s' % att.name()
      print('Warning:', msg)
      scope.logger.addWarning(msg)
    else:
      constituent_id = scope.constituent_dict.get(constituent.id())
    output_list.append(str(constituent_id))

  # Process item or subitems
  if card_format.subitem_names is not None:
    for name in card_format.subitem_names:
      if group_index is None:
        if item.isDiscrete():
          sub = find_subitem(item,name)
        else:
          sub = item.find(name)
      else:
        sub = find_subgroup_item(item, group_index, name)
      if sub is None:
        msg = 'Did not find item %s' % name
        print('WARNING:', msg)
        #scope.logger.addWarning(msg)
        return None

      sub_list = get_values_as_strings(scope, sub)
      output_list += sub_list

  else:
    item_list = get_values_as_strings(scope, item)
    output_list += item_list

  if card_format.comment is not None:
    output_list.append(card_format.comment + " - " + item.attribute().name())

  # Join output_list into one string
  output_text = ' '.join(output_list)
  output_text += '\n'
  return output_text


# ---------------------------------------------------------------------
def write_items(scope, att, format_list):
  '''
  Writes items for a given attribute and list of formatters
  '''
  # Set context id
  context_id = None
  if 'BoundaryCondition' in att.types():
    context_id = att.userData('BCID').value()
  elif 'Material' in att.types():
    context_id = att.userData("MaterialID").value()
  if context_id is not None:
    context_id = str(context_id)

  for card_format in format_list:
    att_temp = att
    # Handle multi-value as special case
    if card_format.card_type == CardType.MULTIVAL:
      write_multivalue_card(scope, att_temp, card_format)
      continue
    #print("Card format ", card_format.item_name)

    # If an item path is passed in, use the itemAtPath to find the parent attribute
    if card_format.item_path is not None:
      if card_format.subitem_names is None:
        item = att_temp.itemAtPath(card_format.item_path+'/'+card_format.item_name,'/')
      else:
        item = att_temp.itemAtPath(card_format.item_path,'/')
    else:
      item = att_temp.find(card_format.item_name)

    #print("Card format name ", card_format.item_name)
    #print("Item name ", item.name())

    if item is None:
      msg = 'item %s not found' % card_format.item_name
      print('WARNING:', msg)
      scope.logger.addWarning(msg)
      continue

    if not item.isEnabled():  # skip items that aren't enabled
      continue

    # Check that item is in an enabled category
    if not item.categories().passes(set(scope.categories)):
      continue

    if item is None:
      msg = 'Unrecognized type for item %s' % item.name()
      print('WARNING:', msg)
      scope.logger.addWarning(msg)
      continue

    # Check for custom writer
    if card_format.custom_writer is not None:
      #print('Calling custom writer: %s()' % card_format.custom_writer.__name__)
      card_format.custom_writer(scope, item, card_format, context_id)
      continue

    # For IDCONVAL cards, call separate function
    if card_format.card_type == CardType.IDCONVAL:
      #write_idconval_cards(scope, concrete_item, card_format, context_id)
      #print(card_format.card_type)
      continue

    # Check if item is group with multiple sub groups
    if item.type() == smtk.attribute.Item.GroupType:
      n = item.numberOfGroups()
      for i in range(n):
        output_text = render_card(scope, item, card_format, \
          context_id=context_id, group_index=i)
        if output_text is not None:
          scope.output.write(output_text)
      continue

    output_text = render_card(scope, item, card_format, context_id)
    if output_text is not None:
      scope.output.write(output_text)
  return True


# ---------------------------------------------------------------------
def write_idconval_cards(scope, item, card_format, context_id):
  '''Writes idconval cards, one for each constituent

  Value comes either from "DefaultValue" or "IndividualValue" items.
  This function is hard-coded to a specific Item name hierachy
  '''
  # Get default value
  default_value = 0.0
  child_item = item.find('DefaultValue')
  if child_item is not None:
    default_value = child_item.value(0)

  # Create dictionary of <constituent id, value>
  # Set each entry to default_value
  val_dict = dict()
  for att_id in scope.constituent_dict.keys():
    val_dict[att_id] = default_value

  # Traverse individual values (if any) and overwrite default values
  child_item = item.find('IndividualValue')
  n = child_item.numberOfGroups()
  for i in range(n):
    constituent_ref = find_subgroup_item(child_item, i, 'Constituent')
    constituent = constituent_ref.value(0)
    constituent_id = constituent.id()

    value_item = find_subgroup_item(child_item, i, 'Value')
    value = value_item.value(0)
    val_dict[constituent_id] = value

  # Traverse constituents in *output* index order
  sorted_by_index = sorted(scope.constituent_dict.items(), key=lambda t:t[1])
  for con_id, con_index in sorted_by_index:
    val = val_dict[con_id]
    output_text = '%s %s %s %s\n' % \
      (card_format.opcode, context_id, con_index, val)
    scope.output.write(output_text)


# ---------------------------------------------------------------------
def write_multivalue_card(scope, att, card_format):
  '''Writes one card using data from a list of attribute items
  '''
  # card_format.item_name is a list for multivalue cards, so
  output_list = list()
  output_list.append(card_format.opcode)

  for item_name in card_format.item_name:
    item = att.find(item_name)
    if item is None:
      msg = 'item %s not found' % card_format.item_name
      print('WARNING:', msg)
      scope.logger.addWarning(msg)
      return False

    output_list += get_values_as_strings(scope, item)

  # Join output_list into one string
  output_string = ' '.join(output_list)
  scope.output.write(output_string)
  scope.output.write('\n')
  return True


# ---------------------------------------------------------------------
def get_function_id(scope, exp_att, units):
  '''
  Returns function #ID for smtk.attribute.Attribute of type SimExpression,
  creating one if needed.
  '''
  name = exp_att.name()
  fcn_id = scope.function_dict.get(name)
  if fcn_id is None:
    if units:
      units = units.value(0)
    if len(scope.function_dict)==0:
      fcn_id = len(set(scope.material_dict.values())) + 1
    else:
      fcn_id = len(scope.function_dict) + len(set(scope.material_dict.values())) +  1
    #print('Assign %d to function %s' % (fcn_id, name))
    scope.function_dict[name] = [fcn_id,units]
  return fcn_id


# ---------------------------------------------------------------------
def get_constant_function_id(scope, value, units=None):
  # Check if function already created for this value
  fcn_id = scope.constant_function_dict.get((value,units))
  if fcn_id is None:
    if len(scope.function_dict)==0:
      fcn_id = len(set(scope.material_dict.values())) + 1
    else:
      fcn_id = len(scope.function_dict) + len(set(scope.material_dict.values())) + 1
    scope.constant_function_dict[value,units] = fcn_id
    # Add placeholder in main function_dict
    scope.function_dict[value,units] = fcn_id
    #print('Assign function id %d to value %s' % (fcn_id, value))
  return fcn_id

# ---------------------------------------------------------------------
@output_writer
def write_functions(scope):
  '''
  Writes functions, using info captured in scope.function_dict
  '''
  if not scope.function_dict:  # empty
    return

  # Get TimestepSize expression
  timestep_expression = None
  time_att = scope.sim_atts.findAttribute('Time')
  if time_att:
    item = time_att.find('TimestepSize')
    if item:
      if item.isExpression():
        timestep_expression = item.expression()

  fcn_list = scope.sim_atts.findAttributes('SimExpression')
  fcn_list.sort(key=lambda f: f.name())
  for f in fcn_list:
    if f.type() != 'XYSeries':
      msg = 'Unrecognized function type %s - not written' % f.type()
      print('WARNING:', msg)
      scope.logger.addWarning(msg)
      continue
    #f.references()[0].owningItem().owningItem().find('Units').value()

    name = f.name()
    xy_fcn = scope.function_dict.get(name)
    if xy_fcn is None:
      continue

    fcn_id = xy_fcn[0]
    fcn_units = xy_fcn[1]

    scope.output.write('! function: %s\n' % name)

    val_pairs_item = f.find('ValuePairs')
    val_pairs_group = smtk.attribute.GroupItem.CastTo(val_pairs_item)
    x_item = val_pairs_group.find('X')
    x_item = smtk.attribute.DoubleItem.CastTo(x_item)
    val_item = val_pairs_group.find('Value')
    val_item = smtk.attribute.DoubleItem.CastTo(val_item)
    num_vals = x_item.numberOfValues()

    scope.output.write('XY1 %d %d %d 0\n' % (fcn_id, num_vals, fcn_units))
    for i in range(num_vals):
      scope.output.write('%f %f\n' % (x_item.value(i), val_item.value(i)))

  # Write constant functions
  if scope.constant_function_dict:
    # needed for converting to/from different time units
    to_seconds = {0: 1, 1: 60, 2: 3600, 3: 86400, 4: 604800, 5: 2629744, 6: 31556926}

    # Get the start time (T0) value and units
    group_item = time_att.findGroup('StartTime')
    value_item = group_item.find('Value')
    start_time = value_item.value()
    units_item = group_item.find('Units')
    start_units = units_item.value()

    # Get the end time (TF) value and units
    group_item = time_att.findGroup('EndTime')
    value_item = group_item.find('Value')
    end_time = value_item.value()
    units_item = group_item.find('Units')
    end_units = units_item.value()

    # Write functions
    for value,fcn_id in scope.constant_function_dict.items():
      val_string = ('%f' % value[0]).rstrip('0').rstrip('.')
      if value[1]:
        units_conv = value[1]
      else:
        units_conv = 0
      scope.output.write('!  constant-valued function: %s\n' % val_string)
      scope.output.write('XY1 %d 2 %d %d\n' % \
        (fcn_id, units_conv, 0))
      # Convert start and end time to the specified function units
      scope.output.write('%f %f\n' % (start_time*to_seconds[start_units]/to_seconds[units_conv], value[0]))
      scope.output.write('%f %f\n' % (end_time*to_seconds[end_units]/to_seconds[units_conv], value[0]))


# ---------------------------------------------------------------------
@output_writer
def write_bc_sets(scope):
  '''
  Writes node/element set for each boundary condition attribute
  '''

  # for efficiency, this function has been implemented in C++
  exportBoundaryConditions = smtk.simulation.adh.ExportBoundaryConditions()
  return exportBoundaryConditions(scope)

# ---------------------------------------------------------------------
@output_writer
def write_MID_cards(scope):
  '''
  Writes material id cards for all model regions
  '''
  #print(scope.material_dict)
  model_manager = scope.model.resource()
  #loop over all of the materials to create a MP MID card for each
  #material/geometry partition pair
  for mat_att in scope.sim_atts.findAttributes("Material"):
    #grab the material attribute
    #get the associated geometry groups
    mat_group_ids = mat_att.associatedModelEntityIds()
    for mat_group_id in mat_group_ids:
      #for each group, grab the associated volumes and their partition id
      mat_group = smtk.model.Group(model_manager.findEntity(mat_group_id))
      vols = mat_group.members()
      for vol in vols:
        scope.output.write('MP MID %s %s\n' % (mat_att.userData("MaterialID").value(), vol.integerProperty('PartitionID')[0]))


# ---------------------------------------------------------------------
def get_values_as_strings(scope, item):
  '''
  Returns list of strings for input item
  '''
  output_list = list()

  # Traverse data contained in this item
  # Each datum can be a value, discrete index, or function id
  if hasattr(item, 'numberOfValues'):
    n = item.numberOfValues()
    for i in range(n):
      #check to see if the item is an expression or a double that allows expressions
      if not (hasattr(item, 'isExpression') and item.isExpression()) and not (item.type() == smtk.attribute.Item.DoubleType and \
        item.allowsExpressions()):
        output_list.append('%s' % item.value(i))
      else:

        #check to see if it is an expression of a constant function
        #if it is a constant function, create the function from the double value
        if hasattr(item, 'isExpression') and item.isExpression():
          exp_att = item.expression()
          exp_id = get_function_id(scope, exp_att)
          output_list.append('%d' % exp_id)
          break # (only 1 expression per item)
        else:
          exp_id = get_constant_function_id(scope,item.value(i))
          output_list.append('%d' % exp_id)
        # if item.owningItem()!=None:
        #   # currently most of our expressions have units associated with them
        #   # this looks to see if there's a special name for the Units attribute
        #
        #   if item.name() != "Value" and item.name().find('Value') != -1:
        #     units_name = item.name()[0] + 'Units'
        #   else:
        #     units_name = 'Units'
        #   units = smtk.to_concrete(item.owningItem()).find(units_name)

  return output_list


# ---------------------------------------------------------------------
def find_active_child(scope, item, name):
  '''Searches item's active children for given name

  Returns None if not found
  '''
  if not hasattr(item, 'activeChildItem'):
    msg = 'Item %s has no active children' % item.name()
    print('WARNING:', msg)
    scope.logger.addWarning(msg)
    return None

  n = item.numberOfActiveChildrenItems()
  for i in range(n):
    child = item.activeChildItem(i)
    if child.name() == name:
      return child
  # If not found, return None
  return None


# ---------------------------------------------------------------------
def find_subgroup_item(group_item, group_index, item_name):
  '''Finds item in one subgroup.

  Returns None if not found.
  This function is needed because GroupItem.find(size_t, std::string)
  returns a const Item, and we need a non-const Item
  '''
  n = group_item.numberOfItemsPerGroup()
  for i in range(n):
    item = group_item.item(group_index, i)
    if item.name() == item_name:
      return item
  # else
  return None

def find_subitem(item,subitem_name):
  '''Finds subitem of discrete attribute'''
  for i in range(item.numberOfActiveChildrenItems()):
    subitem = item.activeChildItem(i)
    if subitem.name() == subitem_name:
      return subitem
  return None

# ---------------------------------------------------------------------
# Assigns integer ids to model entities of given dimension
# Returns the NEXT UNUSED ID. So if no ids are assigned, returns first
def assign_model_entity_ids(model, dimension, material_dict, property_name='id', first=1):
  celltype_dict = {
    0: smtk.model.VERTEX,
    1: smtk.model.EDGE,
    2: smtk.model.FACE,
    3: smtk.model.VOLUME
  }
  celltype = celltype_dict.get(dimension)
  if celltype is None:
    print('Unrecognized dimension', dimension)
    return first

  face_list = model.findEntitiesOfType(smtk.model.VOLUME,True)
  numModelEntities = 0
  for face in face_list:
    entity = face.entity()
    matId = material_dict[str(entity)]
    model.setIntegerProperty(entity, property_name, matId)
    print('Assigned \"%s\" %d to model entity %s (dimension %d)' % \
       (property_name, matId, entity, dimension))
    numModelEntities+=1

  return numModelEntities
####################### my code
#  if smtk.wrappingProtocol() == 'pybind11':
#    entity_list = model.entitiesMatchingFlags(int(celltype), True)
#  else:
#    entity_list = model.entitiesMatchingFlags(celltype, True)
#  entity_id = first
#  for entity in entity_list:
#    model.setIntegerProperty(entity, property_name, entity_id)
    # print('Assigned \"%s\" %d to model entity %s (dimension %d)' % \
    #   (property_name, entity_id, entity, dimension))
#    entity_id += 1

#  return entity_id

# ---------------------------------------------------------------------
@output_writer
def write_hotstart(scope):
  '''Writes hotstart file

  Initial implementation limited to ioh, icon mesh field
  '''

  # for efficiency, this function has been implemented in C++
  exportHotStartFile = smtk.simulation.adh.ExportHotStartFile()
  return exportHotStartFile(scope)

# ---------------------------------------------------------------------
@output_writer
def write_vegetation(scope):
  '''Writes vegetation file
  '''

  exportVegetation = smtk.simulation.adh.ExportVegetation()
  return exportVegetation(scope)

# ---------------------------------------------------------------------
@output_writer
def write_met(scope):
  '''Writes met file

  Initial implementation limited to copying from original location
  to newly created directory
  '''
  print('write_met()')

  # Get start time and units for use below
  gl = scope.sim_atts.findAttribute("Globals")
  met_file = gl.find('MetFileName')

  out_filename = scope.output_filebase + '.met'
  out_path = os.path.join(scope.output_directory, out_filename)

  shutil.copy(met_file.value(),out_path)
  print('Wrote %s' % out_path)
  completed = True
  return completed
