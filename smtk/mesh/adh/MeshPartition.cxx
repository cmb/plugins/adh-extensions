//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================

#include "smtk/mesh/adh/MeshPartition.h"

#include "smtk/model/EntityRef.h"
#include "smtk/model/Group.h"

#include "smtk/simulation/UserData.h"

namespace smtk
{
namespace mesh
{
namespace adh
{

MeshPartition::MeshPartition(const smtk::mesh::MeshSet& ms, int partitionId,
                           smtk::mesh::DimensionType dim)
  : m_meshSet(ms)
  , m_dim(dim)
  , m_partitionId(partitionId)
{
}

std::size_t MeshPartition::numCells() const
{
  return (m_dim == DimensionType_MAX ? m_meshSet.cells().size() : m_meshSet.cells(m_dim).size());
}

smtk::mesh::CellSet MeshPartition::cells(const smtk::mesh::CellType& type) const
{
  return m_meshSet.cells(type);
}

smtk::mesh::CellSet MeshPartition::cells() const
{
  return (m_dim == DimensionType_MAX ? m_meshSet.cells() : m_meshSet.cells(m_dim));
}

std::vector<MeshPartition> partitionByModelEntity(
  smtk::mesh::ResourcePtr meshResource, smtk::mesh::DimensionType type)
{
  std::vector<MeshPartition> meshPartitions;
  smtk::mesh::MeshSet meshes = meshResource->meshes(type);
  if (meshes.is_empty())
  { //if we have no meshes, stop
    return meshPartitions;
  }

  smtk::model::EntityRefArray entities;
  meshes.modelEntities(entities);

  if (entities.size() <= 1)
  { //explicitly done so that no model relationship is equal to everything
    //being in the same partition
    meshPartitions.push_back(MeshPartition(meshes, 1, type));
  }
  else
  {
    meshPartitions.reserve(entities.size());
    int partition = 1;

    typedef smtk::model::EntityRefArray::const_iterator it;
    for (it i = entities.begin(); i != entities.end(); ++i)
    {
      smtk::mesh::MeshSet subset = meshResource->findAssociatedMeshes(*i, type);
      if (!subset.is_empty())
      {
        meshPartitions.push_back(MeshPartition(subset, partition, type));
        ++partition;
      }
    }
  }
  return meshPartitions;
}

std::vector<MeshPartition> partitionByModelProperty(smtk::mesh::ResourcePtr meshResource,
                                                    smtk::model::ResourcePtr resource,
                                                    const std::string& modelPropertyName,
                                                    smtk::mesh::DimensionType type)
{
  std::vector<MeshPartition> meshPartitions;
  smtk::mesh::MeshSet meshes = meshResource->meshes(type);
  if (meshes.is_empty())
  { //if we have no meshes, stop
    return meshPartitions;
  }

  smtk::model::EntityRefArray entities;
  meshes.modelEntities(entities);

  typedef smtk::model::EntityRefArray::const_iterator it;
  for (it i = entities.begin(); i != entities.end(); ++i)
  {
    const smtk::model::IntegerList& values =
      resource->integerProperty(i->entity(), modelPropertyName);
    smtk::mesh::MeshSet subset = meshResource->findAssociatedMeshes(*i, type);
    if (values.size() == 1 && !subset.is_empty())
    { //only accept model properties that have single values
      //since that is what we think partition ids should be
      const int& partition = values[0];
      meshPartitions.push_back(MeshPartition(subset, partition, type));
    }
  }

  return meshPartitions;
}

std::vector<MeshPartition> partitionByAttributeAssociation(
  smtk::mesh::ResourcePtr meshResource,
  const std::set<smtk::attribute::AttributePtr>& attributes,
  smtk::mesh::DimensionType type)
{
  std::vector<MeshPartition> meshPartitions;
  smtk::mesh::MeshSet meshes =
    (type == DimensionType_MAX ? meshResource->meshes() : meshResource->meshes(type));
  if (meshes.is_empty())
  { //if we have no meshes, stop
    return meshPartitions;
  }

  for (auto& attribute : attributes)
  {
    if (auto bcid =
        std::dynamic_pointer_cast<smtk::simulation::UserDataInt>(attribute->userData("BCID")))
    {
      auto associations = attribute->associations();

      // If groups are associated with the attribute, loop over the members of
      // the group.
      std::set<smtk::model::EntityRef> entities;
      for (auto it = associations->begin(); it != associations->end(); ++it)
      {
        auto entity = std::dynamic_pointer_cast<smtk::model::Entity>(*it);
        if (entity->isGroup())
        {
          auto g = smtk::model::Group(entity);
          auto members = g.members<std::vector<smtk::model::EntityRef>>();
          for (auto member : members)
          {
            entities.insert(member);
          }
        }
        else
        {
          entities.insert(entity);
        }
      }
      for (auto i = entities.begin(); i != entities.end(); ++i)
      {
        // If the dimension was unspecified, do not filter by dimension
        if (type == DimensionType_MAX)
        {
          smtk::mesh::MeshSet subset = meshResource->findAssociatedMeshes(*i);
          if (!subset.is_empty())
          {
            meshPartitions.emplace_back(MeshPartition(subset, bcid->value(), DimensionType_MAX));
          }
        }
        else
        {
          // If the dimension was specified, filter by dimension
          smtk::mesh::MeshSet subset = meshResource->findAssociatedMeshes(*i, type);
          if (!subset.is_empty())
          {
            meshPartitions.emplace_back(MeshPartition(subset, bcid->value(), type));
          }
        }
      }
    }
  }

  return meshPartitions;
}

}
}
}
