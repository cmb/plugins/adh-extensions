//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/mesh/adh/operators/AnnotateCanonicalIndex.h"

#include "smtk/common/UUID.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DoubleItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/GroupItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/StringItem.h"

#include "smtk/io/ModelToMesh.h"
#include "smtk/io/ReadMesh.h"

#include "smtk/mesh/core/CellField.h"
#include "smtk/mesh/core/Component.h"
#include "smtk/mesh/core/ForEachTypes.h"
#include "smtk/mesh/core/Resource.h"
#include "smtk/mesh/utility/Create.h"

#include <array>
#include <functional>

int AnnotateCanonicalIndex(int argc, char* argv[])
{
  // Create a new mesh mesh resource
  smtk::mesh::ResourcePtr meshResource = smtk::mesh::Resource::create();

  // Construct a uniform grid
  std::array<std::size_t, 3> discretization = { { 2, 2, 2 } };
  std::function<std::array<double, 3>(std::array<double, 3>)> transform =
    [](std::array<double, 3> x) { return x; };
  auto meshes = smtk::mesh::utility::createUniformGrid(meshResource, discretization, transform);

  // Create an "Annotate Canonical Index" operator
  smtk::operation::Operation::Ptr annotateCanonicalIndex =
    smtk::mesh::adh::AnnotateCanonicalIndex::create();
  if (!annotateCanonicalIndex)
  {
    std::cerr << "No \"annotate canonical index\" operator\n";
    return 1;
  }

  // Loop over the surface meshsets
  for (int i = 1; i < 7; i++)
  {
    // Annotate the i-th mesh face
    annotateCanonicalIndex->parameters()->removeAllAssociations();
    annotateCanonicalIndex->parameters()->associate(smtk::mesh::Component::create(meshes[i]));

    // Execute "annotate boundary conditions" operator...
    smtk::operation::Operation::Result annotateCanonicalIndexResult =
      annotateCanonicalIndex->operate();

    // ...and test the results for success.
    if (annotateCanonicalIndexResult->findInt("outcome")->value() !=
        static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED))
    {
      std::cerr << "\"annotate canonical index\" operator failed\n";
      return 1;
    }

    // Access all of the cell fields on the surface mesh
    std::set<smtk::mesh::CellField> cellFields = meshes[i].cellFields();

    // Check that there is exactly one field (the canonical index)
    if (cellFields.size() != 1)
    {
      std::cerr << "Expected 1 cell field (acquired "<<cellFields.size() << ")\n";
      return 1;
    }

    // Access the canonical index
    smtk::mesh::CellField canonicalIndexField = *cellFields.begin();

    std::vector<int> canonicalIndices = canonicalIndexField.get<int>();
    std::cout<<"canonical indices for boundary "<<i<<": [ ";
    for (auto id : canonicalIndices)
    {
      std::cout<<id<<" ";
    }
    std::cout<<"]"<<std::endl;
  }

  return 0;
}
