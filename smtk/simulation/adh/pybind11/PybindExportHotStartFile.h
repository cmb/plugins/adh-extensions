//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_simulation_adh_ExportHotStartFile_h
#define pybind_smtk_simulation_adh_ExportHotStartFile_h

#include <pybind11/pybind11.h>

#include "smtk/simulation/adh/ExportHotStartFile.h"

#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Resource.h"

namespace py = pybind11;

py::class_< smtk::simulation::adh::ExportHotStartFile > pybind11_init_smtk_adh_ExportHotStartFile(py::module &m)
{
  py::class_< smtk::simulation::adh::ExportHotStartFile > instance(m, "ExportHotStartFile");
  instance
    .def(py::init<>())
    .def("__call__", (void (smtk::simulation::adh::ExportHotStartFile::*)(pybind11::object)) &smtk::simulation::adh::ExportHotStartFile::operator())
    .def("__call__", (void (smtk::simulation::adh::ExportHotStartFile::*)(const std::string&, smtk::mesh::ResourcePtr&, double, std::size_t)) &smtk::simulation::adh::ExportHotStartFile::operator())
    ;
  return instance;
}

#endif
